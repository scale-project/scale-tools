### copy.sh - How to depoy the project
The `copy.sh` file is used to copy secrets ad api keys to their desired position when deploying the project. This is a tutorial to deploy this project

#### Steps
1. download all scale repostories (everything in the scale-project group) into one folder, including this one
2. need to create another folder (usually another repo) which is has your api keys and secrets, mode details [down](#keys-and-secrets-directory)
3. run `copy.sh` in the folder from step 2 and supply only the name of the secrets/keys folder. Example: `./copy.sh ../../scale-secrets`

#### Keys and secrets directory
This is a directory which has the keys and secrets of your fork of scale (its a private repo in my case, called scale-secrets)
The provided program that is run in step 3 is used to copy the keys to the given folder path
Example in my case:

``` bash
project-root
|
+--scale
|  |
|  ...
|
+--scale-api
|  |
|  ...
|
+--scale-tools
|  |
|  +--manage-secrets
|  |  |
|  |  +--copy.sh
|  |  |
|  |  ...
|  |
|  ...
|
+--scale-secrets (keys and secrets repo)
   |
   +--scale-api/credentials-google-api.json
   |
   +--scale-tools/google_api_manipulation/credentials-api.json

```

When the program is run and is supplied with `../../scale-secrets` folder as parameter, it will copy the .json credential files
In my exmaple it will copy `credentials-google-api.json` to `project-scale/scale-api/`
and it will copy `credentials-api.json` to `project-scale/scale-tools/google_api_manipulation/`
and like that for everything in the `scale-secrets` folder

If you need to delete the files copied files, you can just add `clean` as the second parameter to the `copy.sh` files.

The program itself only blindly copying the files to the provided folder stucture so it can easily be used in your fork.

For you first deployment of the project, follow the povided folder strucutre of the scale-secrets folder.
